﻿/*
  CSCI 420 Computer Graphics, USC
  Assignment 1: Height Fields
  C++ starter code

  Student username: <type your USC username here>
*/

#include <iostream>
#include <cstring>
#include "openGLHeader.h"
#include "glutHeader.h"

#include "imageIO.h"
#include "openGLMatrix.h"
#include "basicPipelineProgram.h"
#include <vector>
#include <ctime>

#ifdef WIN32
  #ifdef _DEBUG
    #pragma comment(lib, "glew32d.lib")
  #else
    #pragma comment(lib, "glew32.lib")
  #endif
#endif

#ifdef WIN32
  char shaderBasePath[1024] = SHADER_BASE_PATH;
#else
  char shaderBasePath[1024] = "../openGLHelper-starterCode";
#endif

using namespace std;
BasicPipelineProgram* pipelineProgram;
GLuint buffer;
GLuint bufferLines;
GLuint bufferTri;
float * positionPoints;
float * positionLines;
float * positionTriangles;
float * colors;
float * colorLines;
double * colorTri;
string filename;
const char * imgFile;
int picNum = 0;
OpenGLMatrix* matrix;
GLfloat theta[3] = { 0.0, 0.0, 0.0 };
GLuint vao;
GLuint vaoLines;
GLuint vaoTri;
bool isGLPoints = true;
bool isGLLines = false;
bool isGLTriangles = false;

int mousePos[2]; // x,y coordinate of the mouse position

int leftMouseButton = 0; // 1 if pressed, 0 if not 
int middleMouseButton = 0; // 1 if pressed, 0 if not
int rightMouseButton = 0; // 1 if pressed, 0 if not

typedef enum { ROTATE, TRANSLATE, SCALE } CONTROL_STATE;
CONTROL_STATE controlState = ROTATE;

// state of the world
float landRotate[3] = { 0.0f, 0.0f, 0.0f };
float landTranslate[3] = { 0.0f, 0.0f, 0.0f };
float landScale[3] = { 1.0f, 1.0f, 1.0f };

int windowWidth = 1280;
int windowHeight = 720;
char windowTitle[512] = "CSCI 420 homework I";
void init();
ImageIO * heightmapImage;

vector<float> trackPositions;
GLuint texHandle;
GLuint skyBoxTexHandle[5];
GLuint trackTexHandle;
GLuint bufferGround;
GLuint vaoGround;
GLuint bufferSky;
GLuint vaoSky;
// represents one control point along the spline 
struct Point
{
	double x;
	double y;
	double z;
};

// spline struct 
// contains how many control points the spline has, and an array of control points 
struct Spline
{
	int numControlPoints;
	Point * points;
	vector<Point> pointVector;
	vector<Point> tangentVector;
	vector<Point> normalVector;
	vector<Point> binormalVector;
};

// the spline array 
Spline * splines;
// total number of splines 
int numSplines;
Point arbitrary;
vector<Spline> catSplines;
float width = 50.0f;
float height = 50.0f;
float length = 50.0f;
//creating skyboxes and ground
vector<float> posGround = {
	-width, -height, -length,
	width, -height, -length,
	-width, -height, length,
	width, -height, length,
	
};
vector<float> uvGround = {
	0.0f, 0.0f,
	1.0f, 0.0f,
	0.0f, 1.0f,
	1.0f, 1.0f,
};

vector<float> skyBoxTop = {
	width, height, -length,
	-width, height, -length,
	width, height, length, 
	-width, height, length,
};
vector<float> skyBoxUVTop = {
	1.0f, 0.0f,
	0.0f, 0.0f,
	1.0f, 1.0f,
	0.0f, 1.0f,
};

vector<float> skyBoxFront = {
	 width, height, length,
	 -width, height, length, 
	 width, -height, length, 
	 -width, -height, length,
};
vector<float> skyBoxUVFront = {
	0.0f, 0.0f,
	1.0f, 0.0f,
	0.0f, 1.0f,
	1.0f, 1.0f,
};

vector<float> skyBoxBack = {
	width, -height, -length,
	-width, -height, -length,
	width, height, -length,
	-width, height, -length,
};
vector<float> skyBoxUVBack = {
	0.0f, 1.0f,
	1.0f, 1.0f,
	0.0f, 0.0f,
	1.0f, 0.0f,
	
};

vector<float> skyBoxLeft = {
	-width, height, -length,
	-width, -height, -length,
	-width, height, length,
	-width, -height, length,
};
vector<float> skyBoxUVLeft = {
	1.0f, 0.0f,
	1.0f, 1.0f,
	0.0f, 0.0f,
	0.0f, 1.0f,
};

vector<float> skyBoxRight = {
	width, height, -length,
	width, height, length,
	width, -height, -length,
	width, -height, length,
};
vector<float> skyBoxUVRight = {
	0.0f, 0.0f,
	1.0f, 0.0f,
	0.0f, 1.0f,
	1.0f, 1.0f,
};
vector<float> skyBoxPositions;
vector<float> skyBoxUVs;

int currentPointOnSpline = 0;
int currentSpline = 0;
bool isAnimating = false;
float duration = 0;
float animation = 0;
vector<float> newTrack;//stores the points as Points;
vector<float> trackUVs;
//vector<float> trackPositions;//stores points as Floats;

struct Point CatmullRom(float t, struct Point p1, struct Point p2, struct Point p3, struct Point p4)
{
	float t2 = t*t;
	float t3 = t*t*t;
	struct Point v;

	v.x = (((-t3 + 2 * t2 - t)*(p1.x) + (3 * t3 - 5 * t2 + 2)*(p2.x) + (-3 * t3 + 4 * t2 + t)* (p3.x) + (t3 - t2)*(p4.x)) / 2)*.2;
	v.y = (((-t3 + 2 * t2 - t)*(p1.y) + (3 * t3 - 5 * t2 + 2)*(p2.y) + (-3 * t3 + 4 * t2 + t)* (p3.y) + (t3 - t2)*(p4.y)) / 2)*.2;
	v.z = (((-t3 + 2 * t2 - t)*(p1.z) + (3 * t3 - 5 * t2 + 2)*(p2.z) + (-3 * t3 + 4 * t2 + t)* (p3.z) + (t3 - t2)*(p4.z)) / 2)*.2;

	return v;
}

Point TangentPoint(float u, Point v0, Point v1, Point v2, Point v3){
	Point tan;
	tan.x = 0.5 * ((-v0.x + v2.x) + (2 * v0.x - 5 * v1.x + 4 * v2.x - v3.x) * 2 * u + (-v0.x + 3 * v1.x - 3 * v2.x + v3.x) * 3 * u * u);
	tan.y = 0.5 * ((-v0.y + v2.y) + (2 * v0.y - 5 * v1.y + 4 * v2.y - v3.y) * 2 * u + (-v0.y + 3 * v1.y - 3 * v2.y + v3.y) * 3 * u * u);
	tan.z = 0.5 * ((-v0.z + v2.z) + (2 * v0.z - 5 * v1.z + 4 * v2.z - v3.z) * 2 * u + (-v0.z + 3 * v1.z - 3 * v2.z + v3.z) * 3 * u * u);
	return tan;

}

//cross product
struct Point Cross(Point a, Point b){
	struct Point newPoint;
	newPoint.x = a.y*b.z - a.z*b.y;
	newPoint.y = a.z*b.x - a.x*b.z;
	newPoint.z = a.x*b.y - a.y*b.x;
	return newPoint;
}

//normalize
struct Point unit(Point p3){

	float length_n = sqrt((p3.x*p3.x) + (p3.y*p3.y) + (p3.z*p3.z));
	p3.x = (float)p3.x / length_n;
	p3.y = (float)p3.y / length_n;
	p3.z = (float)p3.z / length_n;

	return p3;

}

// write a screenshot to the specified filename
void saveScreenshot(const char * filename)
{
  unsigned char * screenshotData = new unsigned char[windowWidth * windowHeight * 3];
  glReadPixels(0, 0, windowWidth, windowHeight, GL_RGB, GL_UNSIGNED_BYTE, screenshotData);
  ImageIO screenshotImg(windowWidth, windowHeight, 3, screenshotData);
  if (screenshotImg.save(filename, ImageIO::FORMAT_JPEG) == ImageIO::OK)
    std::cout << "File " << filename << " saved successfully." << endl;
  else std::cout << "Failed to save file " << filename << '.' << endl;

  delete [] screenshotData;
}

void renderQuad()
{
	GLint first = 0;
	glBindVertexArray(vao);
	GLsizei count = newTrack.size()/3;
	glDrawArrays(GL_TRIANGLE_STRIP, first, count);
	glBindVertexArray(0);
}
void bindProgram()
{
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, buffer); // so that
	//glVertexAttribPointer refers to the correct buffer
	GLuint program = pipelineProgram->GetProgramHandle();
	GLuint loc = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(loc);
	const void * offset = 0;
	glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, offset);
	/*GLuint loc2 = glGetAttribLocation(program, "color");
	glEnableVertexAttribArray(loc2);
	offset = (void*)sizeof(positions);
	glVertexAttribPointer(loc2, 4, GL_FLOAT, GL_FALSE, 0, offset);*/
	glBindVertexArray(0);
}
void initPipelineProgram()
{
	pipelineProgram = new BasicPipelineProgram();
	pipelineProgram->Init("../openGLHelper-starterCode");
}

void renderGround(GLuint vao)
{
	GLint first = 0;
	glBindVertexArray(vao);
	GLsizei count = posGround.size() / 3;
	glDrawArrays(GL_TRIANGLE_STRIP, first, count);
	glBindVertexArray(0);
}

void renderSky(GLuint vao, int offset)
{
	GLsizei count = skyBoxBack.size() / 3 ;
	glDrawArrays(GL_TRIANGLE_STRIP, offset, count);
}

void setTextureUnit(GLint unit)
{
	glActiveTexture(unit); // select the active texture unit
	// get a handle to the “textureImage” shader variable
	GLuint program = pipelineProgram->GetProgramHandle();
	GLint h_textureImage = glGetUniformLocation(program, "textureImage");
	// deem the shader variable “textureImage” to read from texture unit “unit”
	glUniform1i(h_textureImage, unit - GL_TEXTURE0);
}

void displayFunc()
{
  // render some stuff...
	glClear(GL_COLOR_BUFFER_BIT |
		GL_DEPTH_BUFFER_BIT);
	matrix->SetMatrixMode(OpenGLMatrix::ModelView);
	matrix->LoadIdentity();

	/*matrix->LookAt(0, 10, 0, 0, 0, 0, 0, 0, -1); // default camera*/
	//matrix->Translate(landTranslate[0], landTranslate[1], landTranslate[2]);
	matrix->Rotate(landRotate[0], 1.0, 0.0, 0.0);
	matrix->Rotate(landRotate[1], 0.0, 1.0, 0.0);
	matrix->Rotate(landRotate[2], 0.0, 0.0, 1.0);
	//matrix->Scale(landScale[0], landScale[1], landScale[2]);
	
	Point eye;
	Point target;
	Point upNormal;

	eye.x = catSplines[currentSpline].pointVector[currentPointOnSpline].x;
	eye.y = catSplines[currentSpline].pointVector[currentPointOnSpline].y;
	eye.z = catSplines[currentSpline].pointVector[currentPointOnSpline].z;

	target.x = catSplines[currentSpline].tangentVector[currentPointOnSpline].x + eye.x;
	target.y = catSplines[currentSpline].tangentVector[currentPointOnSpline].y + eye.y;
	target.z = catSplines[currentSpline].tangentVector[currentPointOnSpline].z + eye.z;

	upNormal.x = catSplines[currentSpline].normalVector[currentPointOnSpline].x;
	upNormal.y = catSplines[currentSpline].normalVector[currentPointOnSpline].y;
	upNormal.z = catSplines[currentSpline].normalVector[currentPointOnSpline].z;
	cout << upNormal.x << endl;
	Point upBinormal = catSplines[currentSpline].binormalVector[currentPointOnSpline];

	matrix->LookAt(eye.x + upNormal.x * 0.03,
		eye.y + upNormal.y * 0.03,
		eye.z + upNormal.z * 0.03,
		target.x,
		target.y,
		target.z,
		upNormal.x, upNormal.y, upNormal.z);

	pipelineProgram->Bind();

	float m[16]; // column-major
	matrix->GetMatrix(m);
	GLboolean isRowMajor = GL_FALSE;
	pipelineProgram->SetModelViewMatrix(m);

	matrix->SetMatrixMode(OpenGLMatrix::Projection);
	matrix->GetMatrix(m);
	pipelineProgram->SetProjectionMatrix(m);
	setTextureUnit(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, trackTexHandle);
	renderQuad();
	//bind ground texture
	setTextureUnit(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texHandle);
	renderGround(vaoGround);
	//bind skybox texture
	glBindVertexArray(vaoSky);
	for (int i = 0; i < 5; i++)
	{
		setTextureUnit(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, skyBoxTexHandle[i]);
		renderSky(vaoSky, i * skyBoxBack.size() / 3);
	}
	glBindVertexArray(0);
	
	glutSwapBuffers();
	/*filename = to_string(picNum);
	filename.append(".jpg");
	imgFile = filename.c_str();
	saveScreenshot(imgFile);
	picNum++;*/
	/*
	pipelineProgram->Bind();
	glBindVertexArray(vao);
	GLint first = 0;
	GLsizei count = 4;
	glDrawArrays(GL_TRIANGLES, first, count);
	glBindVertexArray(0);*/

}

void idleFunc()
{
  // do some stuff... 

  // for example, here, you can save the screenshots to disk (to make the animation)
	std::clock_t start, end, temp;
	//duration += 0.2;
  // make the screen update 
	if (isAnimating)
	{
		duration += 0.1;
		animation += 0.1;
		if (duration > 1 )
		{
			duration = 0;
			currentPointOnSpline++;
			if (currentPointOnSpline > catSplines[currentSpline].pointVector.size() - 1){
				currentSpline++;
				currentPointOnSpline = 0;
				if (currentSpline == catSplines.size() - 1){
					currentSpline = 0;
				}
			}
		}
		/*if (animation > 3)
		{
			animation = 0;
			filename = to_string(picNum);
			filename.append(".jpg");
			imgFile = filename.c_str();
			saveScreenshot(imgFile);
			picNum++;
		}*/
	}
	
  glutPostRedisplay();
}

void reshapeFunc(int w, int h)
{
  glViewport(0, 0, w, h);

  // setup perspective matrix...
  
	GLfloat aspect = (GLfloat)w / (GLfloat)h;
	glViewport(0, 0, w, h);
	matrix->SetMatrixMode(OpenGLMatrix::Projection);
	matrix->LoadIdentity();
	//matrix->Ortho(-2.0, 2.0, -2.0 / aspect, 2.0 / aspect, 0.1, 10.0);
	matrix->Perspective(60.0f, aspect, 0.01f, 100.0f);
	matrix->SetMatrixMode(OpenGLMatrix::ModelView);
  
}

void mouseMotionDragFunc(int x, int y)
{
  // mouse has moved and one of the mouse buttons is pressed (dragging)

  // the change in mouse position since the last invocation of this function
  int mousePosDelta[2] = { x - mousePos[0], y - mousePos[1] };

  switch (controlState)
  {
    // translate the landscape
    case TRANSLATE:
      if (leftMouseButton)
      {
        // control x,y translation via the left mouse button
        landTranslate[0] += mousePosDelta[0] * 0.01f;
        landTranslate[1] -= mousePosDelta[1] * 0.01f;
      }
      if (middleMouseButton)
      {
        // control z translation via the middle mouse button
        landTranslate[2] += mousePosDelta[1] * 0.01f;
      }
      break;

    // rotate the landscape
    case ROTATE:
      if (leftMouseButton)
      {
        // control x,y rotation via the left mouse button
        landRotate[0] += mousePosDelta[1];
        landRotate[1] += mousePosDelta[0];
      }
      if (middleMouseButton)
      {
        // control z rotation via the middle mouse button
        landRotate[2] += mousePosDelta[1];
      }
      break;

    // scale the landscape
    case SCALE:
      if (leftMouseButton)
      {
        // control x,y scaling via the left mouse button
        landScale[0] *= 1.0f + mousePosDelta[0] * 0.01f;
        landScale[1] *= 1.0f - mousePosDelta[1] * 0.01f;
      }
      if (middleMouseButton)
      {
        // control z scaling via the middle mouse button
        landScale[2] *= 1.0f - mousePosDelta[1] * 0.01f;
      }
      break;
  }

  // store the new mouse position
  mousePos[0] = x;
  mousePos[1] = y;
}

void mouseMotionFunc(int x, int y)
{
  // mouse has moved
  // store the new mouse position
  mousePos[0] = x;
  mousePos[1] = y;
}

void mouseButtonFunc(int button, int state, int x, int y)
{
  // a mouse button has has been pressed or depressed

  // keep track of the mouse button state, in leftMouseButton, middleMouseButton, rightMouseButton variables
  switch (button)
  {
    case GLUT_LEFT_BUTTON:
      leftMouseButton = (state == GLUT_DOWN);
    break;

    case GLUT_MIDDLE_BUTTON:
      middleMouseButton = (state == GLUT_DOWN);
    break;

    case GLUT_RIGHT_BUTTON:
      rightMouseButton = (state == GLUT_DOWN);
    break;
  }

  // keep track of whether CTRL and SHIFT keys are pressed
  switch (glutGetModifiers())
  {
    case GLUT_ACTIVE_CTRL:
      controlState = TRANSLATE;
    break;

    case GLUT_ACTIVE_SHIFT:
      controlState = SCALE;
    break;

    // if CTRL and SHIFT are not pressed, we are in rotate mode
    default:
      controlState = ROTATE;
    break;
  }

  // store the new mouse position
  mousePos[0] = x;
  mousePos[1] = y;
}
void initScene(int argc, char *argv[])
{
	// load the image from a jpeg disk file to main memory
	/*heightmapImage = new ImageIO();
	if (heightmapImage->loadJPEG(argv[1]) != ImageIO::OK)
	{
		cout << "Error reading image " << argv[1] << "." << endl;
		exit(EXIT_FAILURE);
	}*/

	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	
}
void keyboardFunc(unsigned char key, int x, int y)
{

  switch (key)
  {
    case 27: // ESC key
      exit(0); // exit the program
    break;

    case ' ':
      std::cout << "You pressed the spacebar." << endl;
    break;

    case 'x':
      // take a screenshot
		filename = "screenshot";
		filename.append("1.jpg");
		imgFile = filename.c_str();
      saveScreenshot(imgFile);
    break;

	case 'd':
		isAnimating = !isAnimating; //d to animate
		
		break;
  }
}



void initVBO()
{
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, newTrack.size() * 4 + trackUVs.size() * 4,
			NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, newTrack.size() * 4, newTrack.data());
		 // init buffer’s size, but don’t assign any data to it
	// upload position data
	//glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(positions), positions);
	// upload color data
	glBufferSubData(GL_ARRAY_BUFFER, newTrack.size() * 4, trackUVs.size() * 4, trackUVs.data());
	

	
}
void initVAO()
{
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao); // bind the VAO
	// bind the VBO “buffer” (must be previously created)
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	// get location index of the “position” shader variable
	GLuint program = pipelineProgram->GetProgramHandle();
	GLuint loc = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(loc); // enable the “position” attribute
	const void * offset = 0; GLsizei stride = 0;
	GLboolean normalized = GL_FALSE;
	// set the layout of the “position” attribute data
	glVertexAttribPointer(loc, 3, GL_FLOAT, normalized, stride, offset);

	GLuint loc2 = glGetAttribLocation(program, "texCoord");
	glEnableVertexAttribArray(loc2);
	offset = (void*)(newTrack.size() * 4);
	glVertexAttribPointer(loc2, 2, GL_FLOAT, GL_FALSE, 0, offset);
	glBindVertexArray(0);
}

void initVBOGround()
{
	glGenBuffers(1, &bufferGround);
	glBindBuffer(GL_ARRAY_BUFFER, bufferGround);
	glBufferData(GL_ARRAY_BUFFER, (posGround.size() + uvGround.size()) * 4,
		NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, posGround.size() * 4, posGround.data());

	glBufferSubData(GL_ARRAY_BUFFER, posGround.size() * 4, uvGround.size() * 4, uvGround.data());
}

void initVAOGround()
{
	glGenVertexArrays(1, &vaoGround);
	glBindVertexArray(vaoGround); // bind the VAO
	// bind the VBO “buffer” (must be previously created)
	glBindBuffer(GL_ARRAY_BUFFER, bufferGround);
	// get location index of the “position” shader variable
	GLuint program = pipelineProgram->GetProgramHandle();
	GLuint loc = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(loc); // enable the “position” attribute
	const void * offset = 0; GLsizei stride = 0;
	GLboolean normalized = GL_FALSE;
	// set the layout of the “position” attribute data
	glVertexAttribPointer(loc, 3, GL_FLOAT, normalized, stride, offset);

	GLuint loc2 = glGetAttribLocation(program, "texCoord");
	glEnableVertexAttribArray(loc2);
	offset = (void*)(posGround.size() * 4);
	glVertexAttribPointer(loc2, 2, GL_FLOAT, normalized, 0, offset);
}

void initVBOSky()
{
	glGenBuffers(1, &bufferSky);
	glBindBuffer(GL_ARRAY_BUFFER, bufferSky);
	glBufferData(GL_ARRAY_BUFFER, (skyBoxPositions.size() + skyBoxUVs.size()) * 4,
		NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, skyBoxPositions.size() * 4, skyBoxPositions.data());

	glBufferSubData(GL_ARRAY_BUFFER, skyBoxPositions.size() * 4, skyBoxUVs.size() * 4, skyBoxUVs.data());
}
void initVAOSky()
{
	glGenVertexArrays(1, &vaoSky);
	glBindVertexArray(vaoSky); // bind the VAO
	// bind the VBO “buffer” (must be previously created)
	glBindBuffer(GL_ARRAY_BUFFER, bufferSky);
	// get location index of the “position” shader variable
	GLuint program = pipelineProgram->GetProgramHandle();
	GLuint loc = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(loc); // enable the “position” attribute
	const void * offset = 0; GLsizei stride = 0;
	GLboolean normalized = GL_FALSE;
	// set the layout of the “position” attribute data
	glVertexAttribPointer(loc, 3, GL_FLOAT, normalized, stride, offset);

	GLuint loc2 = glGetAttribLocation(program, "texCoord");
	glEnableVertexAttribArray(loc2);
	offset = (void*)(skyBoxPositions.size() * 4);
	glVertexAttribPointer(loc2, 2, GL_FLOAT, normalized, 0, offset);
}
int initTexture(const char * imageFilename, GLuint textureHandle)
{
	// read the texture image
	ImageIO img;
	ImageIO::fileFormatType imgFormat;
	ImageIO::errorType err = img.load(imageFilename, &imgFormat);

	if (err != ImageIO::OK)
	{
		printf("Loading texture from %s failed.\n", imageFilename);
		return -1;
	}

	// check that the number of bytes is a multiple of 4
	if (img.getWidth() * img.getBytesPerPixel() % 4)
	{
		printf("Error (%s): The width*numChannels in the loaded image must be a multiple of 4.\n", imageFilename);
		return -1;
	}

	// allocate space for an array of pixels
	int width = img.getWidth();
	int height = img.getHeight();
	unsigned char * pixelsRGBA = new unsigned char[4 * width * height]; // we will use 4 bytes per pixel, i.e., RGBA

	// fill the pixelsRGBA array with the image pixels
	memset(pixelsRGBA, 0, 4 * width * height); // set all bytes to 0
	for (int h = 0; h < height; h++)
		for (int w = 0; w < width; w++)
		{
			// assign some default byte values (for the case where img.getBytesPerPixel() < 4)
			pixelsRGBA[4 * (h * width + w) + 0] = 0; // red
			pixelsRGBA[4 * (h * width + w) + 1] = 0; // green
			pixelsRGBA[4 * (h * width + w) + 2] = 0; // blue
			pixelsRGBA[4 * (h * width + w) + 3] = 255; // alpha channel; fully opaque

			// set the RGBA channels, based on the loaded image
			int numChannels = img.getBytesPerPixel();
			for (int c = 0; c < numChannels; c++) // only set as many channels as are available in the loaded image; the rest get the default value
				pixelsRGBA[4 * (h * width + w) + c] = img.getPixel(w, h, c);
		}

	// bind the texture
	glBindTexture(GL_TEXTURE_2D, textureHandle);

	// initialize the texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixelsRGBA);

	// generate the mipmaps for this texture
	glGenerateMipmap(GL_TEXTURE_2D);

	// set the texture parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// query support for anisotropic texture filtering
	GLfloat fLargest;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);
	printf("Max available anisotropic samples: %f\n", fLargest);
	// set anisotropic texture filtering
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 0.5f * fLargest);

	// query for any errors
	GLenum errCode = glGetError();
	if (errCode != 0)
	{
		printf("Texture initialization error. Error code: %d.\n", errCode);
		return -1;
	}

	// de-allocate the pixel array -- it is no longer needed
	delete[] pixelsRGBA;

	return 0;
}

void init()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClearColor(1.0, 1.0f, 1.0f, 0);
	glEnable(GL_DEPTH_TEST);
	matrix = new OpenGLMatrix();
	//loading all the textures
	glGenTextures(1, &texHandle);
	int code = initTexture("heightmap/background.jpg", texHandle);
	if (code != 0)
	{
		printf("Error loading the texture image.\n");
		exit(EXIT_FAILURE);
	}
	glGenTextures(1, &trackTexHandle);
	code = initTexture("heightmap/skyBack.jpg", trackTexHandle);
	if (code != 0)
	{
		printf("Error loading the texture image.\n");
		exit(EXIT_FAILURE);
	}
	glGenTextures(5, skyBoxTexHandle);
	code = initTexture("heightmap/background.jpg", skyBoxTexHandle[0]);
	if (code != 0)
	{
		printf("Error loading the texture image.\n");
		exit(EXIT_FAILURE);
	}
	code = initTexture("heightmap/background.jpg", skyBoxTexHandle[1]);
	if (code != 0)
	{
		printf("Error loading the texture image.\n");
		exit(EXIT_FAILURE);
	}
	code = initTexture("heightmap/background.jpg", skyBoxTexHandle[2]);
	if (code != 0)
	{
		printf("Error loading the texture image.\n");
		exit(EXIT_FAILURE);
	}
	code = initTexture("heightmap/background.jpg", skyBoxTexHandle[3]);
	if (code != 0)
	{
		printf("Error loading the texture image.\n");
		exit(EXIT_FAILURE);
	}
	code = initTexture("heightmap/background.jpg", skyBoxTexHandle[4]);
	if (code != 0)
	{
		printf("Error loading the texture image.\n");
		exit(EXIT_FAILURE);
	}
	
	initPipelineProgram();
	initVBO();
	initVAO();
	initVBOGround();
	initVAOGround();
	initVBOSky();
	initVAOSky();
}


int loadSplines(char * argv)
{
	char * cName = (char *)malloc(128 * sizeof(char));
	FILE * fileList;
	FILE * fileSpline;
	int iType, i = 0, j, iLength;

	// load the track file 
	fileList = fopen(argv, "r");
	if (fileList == NULL)
	{
		printf("can't open file\n");
		exit(1);
	}

	// stores the number of splines in a global variable 
	fscanf(fileList, "%d", &numSplines);

	splines = (Spline*)malloc(numSplines * sizeof(Spline));

	// reads through the spline files 
	for (j = 0; j < numSplines; j++)
	{
		i = 0;
		fscanf(fileList, "%s", cName);
		fileSpline = fopen(cName, "r");

		if (fileSpline == NULL)
		{
			printf("can't open file\n");
			exit(1);
		}

		// gets length for spline file
		fscanf(fileSpline, "%d %d", &iLength, &iType);

		// allocate memory for all the points
		splines[j].points = (Point *)malloc(iLength * sizeof(Point));
		splines[j].numControlPoints = iLength;

		// saves the data to the struct
		while (fscanf(fileSpline, "%lf %lf %lf",
			&splines[j].points[i].x,
			&splines[j].points[i].y,
			&splines[j].points[i].z) != EOF)
		{
			i++;
		}
	}

	free(cName);

	return 0;
}

void drawTrack()
{
	Point v0, v1, v2, v3, v4, v5, v6, v7;
	float a = 0.005;
	for (auto s : catSplines)
	{
		for (int i = 0; i < s.pointVector.size() - 1; i++)
		{
			Point p0, p1, n0, n1, b0, b1;
			p0 = s.pointVector[i];
			p1 = s.pointVector[i + 1];
			n0 = s.normalVector[i];
			n1 = s.normalVector[i + 1];
			b0 = s.binormalVector[i];
			b1 = s.binormalVector[i + 1];
			//fill in data for the 8 points
			v0.x = p0.x + a * (-n0.x + b0.x);
			v0.y = p0.y + a * (-n0.y + b0.y);
			v0.z = p0.z + a * (-n0.z + b0.z);

			v1.x = p0.x + a * (n0.x + b0.x);
			v1.y = p0.y + a * (n0.y + b0.y);
			v1.z = p0.z + a * (n0.z + b0.z);

			v2.x = p0.x + a * (n0.x - b0.x);
			v2.y = p0.y + a * (n0.y - b0.y);
			v2.z = p0.z + a * (n0.z - b0.z);

			v3.x = p0.x + a * (-n0.x - b0.x);
			v3.y = p0.y + a * (-n0.y - b0.y);
			v3.z = p0.z + a * (-n0.z - b0.z);

			v4.x = p1.x + a * (-n1.x + b1.x);
			v4.y = p1.y + a * (-n1.y + b1.y);
			v4.z = p1.z + a * (-n1.z + b1.z);

			v5.x = p1.x + a * (n1.x + b1.x);
			v5.y = p1.y + a * (n1.y + b1.y);
			v5.z = p1.z + a * (n1.z + b1.z);

			v6.x = p1.x + a * (n1.x - b1.x);
			v6.y = p1.y + a * (n1.y - b1.y);
			v6.z = p1.z + a * (n1.z - b1.z);

			v7.x = p1.x + a * (-n1.x - b1.x);
			v7.y = p1.y + a * (-n1.y - b1.y);
			v7.z = p1.z + a * (-n1.z - b1.z);

			//right face of track
			newTrack.push_back(v0.x);
			newTrack.push_back(v0.y);
			newTrack.push_back(v0.z);
			newTrack.push_back(v1.x);
			newTrack.push_back(v1.y);
			newTrack.push_back(v1.z);
			newTrack.push_back(v4.x);
			newTrack.push_back(v4.y);
			newTrack.push_back(v4.z);
			newTrack.push_back(v5.x);
			newTrack.push_back(v5.y);
			newTrack.push_back(v5.z);
	
			//top face of track
			newTrack.push_back(v1.x);
			newTrack.push_back(v1.y);
			newTrack.push_back(v1.z);
			newTrack.push_back(v2.x);
			newTrack.push_back(v2.y);
			newTrack.push_back(v2.z);
			newTrack.push_back(v5.x);
			newTrack.push_back(v5.y);
			newTrack.push_back(v5.z);
			newTrack.push_back(v6.x);
			newTrack.push_back(v6.y);
			newTrack.push_back(v6.z);

			//left face of track
			newTrack.push_back(v2.x);
			newTrack.push_back(v2.y);
			newTrack.push_back(v2.z);
			newTrack.push_back(v3.x);
			newTrack.push_back(v3.y);
			newTrack.push_back(v3.z);
			newTrack.push_back(v6.x);
			newTrack.push_back(v6.y);
			newTrack.push_back(v6.z);
			newTrack.push_back(v7.x);
			newTrack.push_back(v7.y);
			newTrack.push_back(v7.z);

			//bottom face of track
			newTrack.push_back(v3.x);
			newTrack.push_back(v3.y);
			newTrack.push_back(v3.z);
			newTrack.push_back(v0.x);
			newTrack.push_back(v0.y);
			newTrack.push_back(v0.z);
			newTrack.push_back(v7.x);
			newTrack.push_back(v7.y);
			newTrack.push_back(v7.z);
			newTrack.push_back(v4.x);
			newTrack.push_back(v4.y);
			newTrack.push_back(v4.z);

		}
	}
	for (int i = 0; i < newTrack.size()/3 * 2; i++)
	{
		trackUVs.push_back(0);
	}



}
int main(int argc, char *argv[])
{
  
  if (argc<2)
  {
	  printf("usage: %s <trackfile>\n", argv[0]);
	  exit(0);
  }

  // load the splines from the provided filename
  loadSplines(argv[1]);

  printf("Loaded %d spline(s).\n", numSplines);
  for (int i = 0; i<numSplines; i++)
	  printf("Num control points in spline %d: %d.\n", i, splines[i].numControlPoints);

  std::cout << "Initializing GLUT..." << endl;
  glutInit(&argc,argv);

  cout << "Initializing OpenGL..." << endl;

  #ifdef __APPLE__
    glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);
  #else
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);
  #endif

  glutInitWindowSize(windowWidth, windowHeight);
  glutInitWindowPosition(0, 0);  
  glutCreateWindow(windowTitle);

  std::cout << "OpenGL Version: " << glGetString(GL_VERSION) << endl;
  std::cout << "OpenGL Renderer: " << glGetString(GL_RENDERER) << endl;
  std::cout << "Shading Language Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;

  arbitrary.x = 1;
  arbitrary.y = 0;
  arbitrary.z = 0;

  struct Spline* catSpline;
  
  //std::cout << g_iNumOfSplines << std::endl;
  for (int i = 0; i < numSplines; i++){
	  for (int j = 1; j <= (splines[i].numControlPoints - 2); j++){
		  //std::cout << g_Splines[i].numControlPoints + 3 << std::endl;
		  catSpline = new struct Spline;
		  catSpline->numControlPoints = 51;
		  //catSpline->points = (struct point *)malloc(5 * sizeof(struct point));
		  for (int k = 0; k < 51; k++){
			  double u = k * 0.02;
			  struct Point p1 = splines[i].points[j - 1];
			  struct Point p2 = splines[i].points[j];
			  struct Point p3 = splines[i].points[j + 1];
			  struct Point p4 = splines[i].points[j + 2];

			  struct Point v = CatmullRom(u, p1, p2, p3, p4);

			  struct Point t = TangentPoint(u, p1, p2, p3, p4);
			  t = unit(t);
			  
			  struct Point n;
			  n = Cross(arbitrary, t);
			  n = unit(n);

			  struct Point b;
			  b = Cross(t, n);
			  b = unit(b);
			  arbitrary = b;

			  //std::cout << "x: " << v.x << " y: " << v.y << " z: " << v.z << std::endl;
			  catSpline->pointVector.push_back(v);
			  catSpline->tangentVector.push_back(t);
			  catSpline->normalVector.push_back(n);
			  catSpline->binormalVector.push_back(b);

		  }
		  catSplines.push_back(*catSpline);
	  }
  }

  for (auto s : catSplines)
  {
	  for (int i = 0; i < s.pointVector.size() - 1; i++)
	  {
		  //loading positions of each point into trackPositions
		  trackPositions.push_back(s.pointVector[i].x);
		  trackPositions.push_back(s.pointVector[i].y);
		  trackPositions.push_back(s.pointVector[i].z);
		  trackPositions.push_back(s.pointVector[i + 1].x);
		  trackPositions.push_back(s.pointVector[i + 1].y);
		  trackPositions.push_back(s.pointVector[i + 1].z);
		  //cout << "NORMALS: " << s.normalVector[i].x <<" "<< s.normalVector[i].y <<" "<<s.normalVector[i].z << endl;

	  }
  }
  for (int i = 0; i < catSplines[0].normalVector.size(); i++)
  {
	  cout << "NORMALS: " << catSplines[0].normalVector[i].x << " " << catSplines[0].normalVector[i].y << " "<<catSplines[0].normalVector[i].z << endl;
  }
  //saving positions of skybox
  for (int i = 0; i < skyBoxBack.size(); i++)
  {
	  skyBoxPositions.push_back(skyBoxBack[i]);
  }
  for (int i = 0; i < skyBoxFront.size(); i++)
  {
	  skyBoxPositions.push_back(skyBoxFront[i]);
  }
  for (int i = 0; i < skyBoxLeft.size(); i++)
  {
	  skyBoxPositions.push_back(skyBoxLeft[i]);
  }
  for (int i = 0; i < skyBoxRight.size(); i++)
  {
	  skyBoxPositions.push_back(skyBoxRight[i]);
  }
  for (int i = 0; i < skyBoxTop.size(); i++)
  {
	  skyBoxPositions.push_back(skyBoxTop[i]);
  }
  //saving uv of skybox
  for (int i = 0; i < skyBoxUVBack.size(); i++)
  {
	  skyBoxUVs.push_back(skyBoxUVBack[i]);
  }
  for (int i = 0; i < skyBoxUVFront.size(); i++)
  {
	  skyBoxUVs.push_back(skyBoxUVFront[i]);
  }
  for (int i = 0; i < skyBoxUVLeft.size(); i++)
  {
	  skyBoxUVs.push_back(skyBoxUVLeft[i]);
  }
  for (int i = 0; i < skyBoxUVRight.size(); i++)
  {
	  skyBoxUVs.push_back(skyBoxUVRight[i]);
  }
  for (int i = 0; i < skyBoxUVTop.size(); i++)
  {
	  skyBoxUVs.push_back(skyBoxUVTop[i]);
  }

  drawTrack();

  // tells glut to use a particular display function to redraw 
  glutDisplayFunc(displayFunc);
  // perform animation inside idleFunc
  glutIdleFunc(idleFunc);
  // callback for mouse drags
  glutMotionFunc(mouseMotionDragFunc);
  // callback for idle mouse movement
  glutPassiveMotionFunc(mouseMotionFunc);
  // callback for mouse button changes
  glutMouseFunc(mouseButtonFunc);
  // callback for resizing the window
  glutReshapeFunc(reshapeFunc);
  // callback for pressing the keys on the keyboard
  glutKeyboardFunc(keyboardFunc);

  // init glew
  #ifdef __APPLE__
    // nothing is needed on Apple
  #else
    // Windows, Linux
    GLint result = glewInit();
    if (result != GLEW_OK)
    {
      cout << "error: " << glewGetErrorString(result) << endl;
      exit(EXIT_FAILURE);
    }
  #endif

  // do initialization
  initScene(argc, argv);
 
  init(); // our custom initialization 
  glutMainLoop();
}



